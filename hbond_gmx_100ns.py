import os
import numpy as np
import subprocess
import pickle

'''
hydrogen bond distribution
if -ac used hbac.xvg will be generated. The file has 5 columns. The meaning of the columns are:
#Ac means autocorrelation
* Time
* Ac(hbond) with correction for the fact that a finite system is being simulated.
* Ac(hbond) without correction
* Cross correlation between hbonds and contacts (see the papers by Luzar&Chandler and van der Spoel that are mentioned in the stdout from g_hbond)
* Derivative of second column.
'''

# traj parameters
traj_length = 100000
sampling_dt = 10
gro = 'nvt.gro'
tpr = 'nvt.tpr'
xtc = 'nvt-nopbc-100ns.xtc'
ndx = 'rdf.ndx'
topol = 'dt.top'
start_time = 90000
end_time = 100000

if os.path.exists('HBOND_DA_100ns') == False:
	os.mkdir('HBOND_DA_100ns')
os.chdir('HBOND_DA_100ns')
#read topology 
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../%s' % topol) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
				num_ntc = int(line.split()[1])

#DGA-SOL
if num_wat > 0:
	#hydrogen-acceptor distance 0.35nm (DGA-SOL)
	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time, '-r','0.38','-ac','hbac_3o_sol.xvg','-num','hbnum_3o_sol.xvg','-ang','hbang_3o_sol.xvg','-dist','hbdist_3o_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O1_O2_O3\nSOL\n').encode())[0]

	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o1_sol.xvg','-num','hbnum_o1_sol.xvg','-ang','hbang_o1_sol.xvg','-dist','hbdist_o1_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O1\nSOL\n').encode())[0]

	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o2_sol.xvg','-num','hbnum_o2_sol.xvg','-ang','hbang_o2_sol.xvg','-dist','hbdist_o2_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O2\nSOL\n').encode())[0]

	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o3_sol.xvg','-num','hbnum_o3_sol.xvg','-ang','hbang_o3_sol.xvg','-dist','hbdist_o3_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O3\nSOL\n').encode())[0]

	#hydrogen-acceptor distance 0.25nm (SOL-SOL)
	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_sol_sol.xvg','-num','hbnum_sol_sol.xvg','-ang','hbang_sol_sol.xvg','-dist','hbdist_sol_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('SOL\nSOL\n').encode())[0]

#HNO3-HBOND
if num_ntc > 0:
	#hydrogen-acceptor distance 0.25nm NITRI and SOL
	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_nitri_sol.xvg','-num','hbnum_nitri_sol.xvg','-ang','hbang_nitri_sol.xvg','-dist','hbdist_nitri_sol.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('NITRI\nSOL\n').encode())[0]

	#hydrogen-acceptor distance 0.35nm DGA and NITRI
	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o1_nitri.xvg','-num','hbnum_o1_nitri.xvg','-ang','hbang_o1_nitri.xvg','-dist','hbdist_o1_nitri.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O1\nNITRI\n').encode())[0]

	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o2_nitri.xvg','-num','hbnum_o2_nitri.xvg','-ang','hbang_o2_nitri.xvg','-dist','hbdist_o2_nitri.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O2\nNITRI\n').encode())[0]

	hbond = subprocess.Popen(('gmx_mpi_d','hbond','-f','../%s' % xtc,'-s','../%s' % tpr,'-n','../%s' % ndx,'-da','yes','-b','%s' % start_time,'-e','%s' % end_time,'-r','0.38','-ac','hbac_o3_nitri.xvg','-num','hbnum_o3_nitri.xvg','-ang','hbang_o3_nitri.xvg','-dist','hbdist_o3_nitri.xvg'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = hbond.communicate(input=('DGA_&_O3\nNITRI\n').encode())[0]


#########calculation ends##################
