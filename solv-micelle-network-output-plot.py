import pickle 
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

with open('solvent-micelle-network-c4.p','rb') as f:
	b= pickle.load(f)

composite_list = [b[x:x+6] for x in range(0, len(b),6)]

print('length of list:',len(composite_list))
f_loop = 0
count_same_water = 0
count_same_ext = 0

wat_dim_wat = max(composite_list[each][1][0] for each in range(len(composite_list)) if composite_list[each][2] == composite_list[each-1][2])
print(wat_dim_wat)
ext_dim_wat = max(composite_list[each][3][0] for each in range(len(composite_list)) if composite_list[each][2] == composite_list[each-1][2])

print(ext_dim_wat)

#mat_wat_bridge = np.zeros((wat_dim_wat+1,ext_dim_wat+1))
mat_wat_bridge = np.zeros((50,30))
wat_dim_ext = max(composite_list[each][1][0] for each in range(len(composite_list)) if composite_list[each][4] == composite_list[each-1][4])
print(wat_dim_ext)
ext_dim_ext = max(composite_list[each][3][0] for each in range(len(composite_list)) if composite_list[each][4] == composite_list[each-1][4])

print(ext_dim_ext)
#mat_ext_bridge = np.zeros((wat_dim_ext+1,ext_dim_ext+1))
mat_ext_bridge = np.zeros((50,30))

for each in range(len(composite_list)):
	f_loop += 1
	wat_cluster = composite_list[each][2]
	prev_wat_cluster = composite_list[each-1][2]
	ext_cluster = composite_list[each][4]
	prev_ext_cluster = composite_list[each-1][4]
	time = composite_list[each][0]
	prev_time = composite_list[each-1][0]
	wat_size = composite_list[each][1][0]
	ext_size = composite_list[each][3][0]
    
    #same water pool connected to different extractant clusters 
	if (wat_cluster == prev_wat_cluster) and (time == prev_time) and (ext_cluster != prev_ext_cluster):
		count_same_water += 1
		mat_wat_bridge[wat_size][ext_size] += 1
        
	if (time == prev_time) and (ext_cluster == prev_ext_cluster) and (wat_cluster != prev_wat_cluster):
		count_same_ext += 1
		mat_ext_bridge[wat_size][ext_size] += 1
        
print(count_same_water)
print(count_same_ext)
#fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2,figsize=(10, 8))


fig, (ax1, ax2) = plt.subplots(figsize=(10, 8), ncols=2)
max_x = max(ext_dim_ext, ext_dim_wat)
max_y = max(wat_dim_ext, wat_dim_wat)
pos1 = ax1.imshow(mat_wat_bridge, cmap='pink', vmin = 1, interpolation='nearest',extent=[1, 30, 50, 1])
pos2 = ax2.imshow(mat_ext_bridge, cmap='pink', vmin= 1, interpolation='nearest',extent=[1, 30, 50, 1])

divider1 = make_axes_locatable(ax1)
cax1 = divider1.append_axes("right", size="5%", pad=0.05)

divider2 = make_axes_locatable(ax2)
cax2 = divider2.append_axes("right", size="5%", pad=0.05)

fig.colorbar(pos1,cax=cax1,ticks=[0, 5, 10, 15, 20, 25, 30])
fig.colorbar(pos2,cax=cax2, ticks = [50, 100, 150, 200, 250, 300])


#tick at top x axis
ax1.xaxis.tick_top()
ax2.xaxis.tick_top()

ax1.set_xlabel('Cluster size (TODGA)',fontsize=12, fontweight='bold')
ax1.xaxis.set_label_position('top') 
ax1.set_ylabel('Cluster size (Water)',fontsize=12, fontweight='bold')
ax2.set_xlabel('Cluster size (TODGA)',fontsize=12, fontweight='bold')
ax2.xaxis.set_label_position('top') 
plt.savefig('solv-micelle-C4.png', dpi=600)

#plt.show()
