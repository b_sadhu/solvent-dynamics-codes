import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import pickle
import MDAnalysis as mda

'''
framewise solvent distribution
'''

#directory for files
if os.path.exists('NUM_SOLV') == False:
	os.mkdir('NUM_SOLV')
os.chdir('NUM_SOLV')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../dt.top') as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])


#read the id number of first extractant
with open('../../nvt.gro') as gro:
	lines = gro.readlines()[1:]
	for line in lines:
		if 'DGA' in line:
			id_ext = int(line.split('DGA')[0])
			break

###list of zero in file creation###
def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros

if os.path.exists('ex-framewise-solvent-0-4-10') == False:
	os.mkdir('ex-framewise-solvent-0-4-10')

#load trajectory
u = mda.Universe('../../nvt_extend.tpr','../../nvt-extend-nopbc-10ns.xtc')
counter = 0
num_list = zerolistmaker(10)
for time in u.trajectory[0::10]:
	print('time:',u.trajectory.frame)
	num = 0
	num_list = zerolistmaker(7)
	num_list[0] = u.trajectory.frame
	clust_solv = []
	#id_solv = []
	#3zone defines for coordination shell NTC and WAT
	if num_ntc > 0: 	
		ntr_1num = len(u.select_atoms("resname NITRI and name N and (around 4 resname DGA and name O1 O2 O3)", updating=True).residues)
		num_list[1] = ntr_1num
		ntr_2num = len(u.select_atoms("resname NITRI and name N and (not around 4 resname DGA and name O1 O2 O3) and (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
		num_list[2] = ntr_2num
		ntr_fnum = len(u.select_atoms("resname NITRI and name N and not (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
		print('ntr:',ntr_1num,ntr_2num,ntr_fnum)
		num_list[3] = ntr_fnum
	if num_wat > 0:
		wat_1num = len(u.select_atoms("resname SOL and name OW and (around 4 resname DGA and name O1 O2 O3)", updating=True).residues)
		num_list[4] = wat_1num
		wat_2num = len(u.select_atoms("resname SOL and name OW and (not around 4 resname DGA and name O1 O2 O3) and (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
		num_list[5] = wat_2num
		wat_fnum = len(u.select_atoms("resname SOL and name OW and not (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
		num_list[6] = wat_fnum
		print('water:',wat_1num,wat_2num,wat_fnum)
	print('num_list:',num_list)
	if os.path.exists('ex-framewise-solvent-0-4-10/pic_whole-solvent.txt'):
		with open('ex-framewise-solvent-0-4-10/pic_whole-solvent.txt','rb') as rfp:
			clust_solv = pickle.load(rfp)
	clust_solv.append(num_list)
	with open('ex-framewise-solvent-0-4-10/pic_whole-solvent.txt','wb') as wfp2:
		pickle.dump(clust_solv, wfp2)
	with open('ex-framewise-solvent-0-4-10/whole-solvent.txt', "a+") as fp:
		fp.seek(0)
		data = fp.read(100)
		if len(data) > 0:
			fp.write("\n")
		fp.write(str(num_list))
	#counter += 1
#os.system('rm *.gro')
#########calculation ends##################

