import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import pickle
import MDAnalysis as mda
import _pickle as cPickle
import gc

'''
water micelle cluster connections and corresponding distributions
'''
gc.disable()

# traj parameters
traj_length = 100000
sampling_dt = 100
gro = 'nvt.gro'
tpr = 'nvt.tpr'
xtc = 'nvt-nopbc-100ns.xtc'
ndx = 'rdf.ndx'
topol = 'dt.top'
index_group = 28

#directory for files
if os.path.exists('NUM_SOLV') == False:
	os.mkdir('NUM_SOLV')
os.chdir('NUM_SOLV')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../%s' % topol) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])


#read the id number of first extractant and water (solvent)
with open('../../%s' % gro) as extgrofile:
	lines = extgrofile.readlines()[1:]
	for line in lines:
		if 'NITRI' in line:
			id_ext = int(line.split('NITRI')[0])
			print('id_ext:',id_ext)
			break
with open('../../%s' % gro) as extgrofile:
	lines = extgrofile.readlines()[1:]
	for line in lines:
		if 'SOL' in line:
			id_wat = int(line.split('SOL')[0])
			print('id_wat:',id_wat)
			break

###list of zero in file creation###
def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros

#water cluster_pickle_file and extractant cluster pickle file
with open("../../Cluster_analysis/100ps_cut12_lifetime/ecc-clust-outputs/pic_markov_6.txt", "rb") as sp:
	solv_cluster = cPickle.load(sp)
with open("../../Cluster_analysis/100ps_cut12_lifetime/ecc-clust-outputs/pic_markov_7.txt", "rb") as fp:
	b = cPickle.load(fp)

if os.path.exists('clusterwise-solvent') == False:
	os.mkdir('clusterwise-solvent')

line_count = 0
for each in solv_cluster:
	time = (line_count*sampling_dt)
	print('time:',time)
	num = 0
        #make .gro for each frame
	make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr,'-b','%s' % time, '-e','%s' % time,'-n','../../%s' % ndx,'-o','DGA_%s.gro' % (time)),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = make_gro.communicate(input=('%s \nq\n' % index_group).encode())[0]
	#creating universe file for each frame
	u = mda.Universe('DGA_%s.gro' % (time))
	u.load_new('DGA_%s.gro' % (time))
	for j in range(len(each)):
		listwat = []
		if j > 0:
			prev = len(each[j-1])
			for k in each[j]:
				k += id_wat
				listwat.append(k)
			length = len(listwat)
			idwat = str(listwat)[1:-1].replace( ','  , ' ' )
			if length > 0:
				countlist = []
				countlisttxt = []
				if length == prev:
					num += 1
				else:
					num = 0
				#reading line of extractant cluster file
				for x in range(len(b[line_count])):
					list1 = []
					if x > 0:
						prev = len(b[line_count][x-1])
						for y in b[line_count][x]:
							count = 0
							y += id_ext
							list1.append(y)
							length_dga = len(list1)
							idext = str(list1)[1:-1].replace( ','  , ' ' )
						if length_dga > 0:
							bond_1num = len(u.select_atoms("resid %s and name OW and (around 3.8 resid %s and name O*)" % (str(idwat),str(idext)), updating=True).residues)
							#print(bond_1num)
							if bond_1num > 0:
								if os.path.exists('clusterwise-solvent/solvent-wat-ntc-network.p'):
									with open('clusterwise-solvent/solvent-wat-ntc-network.p','rb') as rfp:
										countlist = cPickle.load(rfp)
								count += 1
								countlist.append([time])
								countlist.append([length])
								countlist.append(listwat)
								countlist.append([length_dga])
								countlist.append(list1)
								countlist.append([count])
								countlisttxt.append("{},{},{},{},{},{}".format([time],[length],listwat,[length_dga],list1,[count]))
								#print(countlist)
								cPickle.dump(countlist, open("clusterwise-solvent/solvent-wat-ntc-network.p", "wb" ), protocol=-1)
								with open('clusterwise-solvent/solv-wat-ntc-network.txt', "a+") as fp:
									fp.seek(0)
									data = fp.read(100)
									if len(data) > 0:
										fp.write("\n")
									fp.write(str(countlisttxt))
						
	line_count += 1
	gc.enable()
#os.system('rm *.gro')
#########calculation ends##################

