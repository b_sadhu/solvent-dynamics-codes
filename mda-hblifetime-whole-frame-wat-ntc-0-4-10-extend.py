import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import pickle
import MDAnalysis as mda
from MDAnalysis.analysis.waterdynamics import HydrogenBondLifetimes as HBL

'''
hydrogen bond lifetime calculation
'''

#directory for files
if os.path.exists('HBL') == False:
	os.mkdir('HBL')
os.chdir('HBL')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../dt.top') as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])

#load trajectory
u = mda.Universe('../../nvt_extend.tpr','../../nvt-extend-nopbc-10ns.xtc')

selection1 = "resname SOL and name OW and (around 4 name O1 O2 O3)"
selection2 = "resname DGA and name O1 O2 O3"

##SOL VS 2ND COORDINATION SHELL


HBL_analysis = HBL(u, selection1, selection1, 0, 400000, 400)
HBL_analysis.run()
time = 0
DHBLC = {}
DHBLI = {}
for HBLc, HBLi in HBL_analysis.timeseries:
	DHBLC[time] = HBLc
	DHBLI[time] = HBLc
	print("{time} {HBLc} {time} {HBLi}".format(time=time, HBLc=HBLc, HBLi=HBLi))
	time += 1
#we can also plot our data
LHB = [DHBLC, DHBLI]
pickle.dump(LHB, open( "LHB.p", "wb" ))

plt.figure(1,figsize=(18, 6))

#HBL continuos
plt.subplot(121)
plt.xlabel('time')
plt.ylabel('Hydrogen bond lifetime')
plt.xlim((0,400))
plt.ylim((0,1))
plt.gca().legend(('TODGA-Water'))
plt.plot(range(0,time),[column[0] for column in HBL_analysis.timeseries])

#HBL intermitent
plt.subplot(122)
plt.xlabel('time')
plt.ylabel('HBLi')
plt.xlim((0,400))
plt.ylim((0,1))
plt.gca().legend(('TODGA-Water'))
plt.plot(range(0,time),[column[1] for column in HBL_analysis.timeseries])
plt.savefig('hbl-dga-wat-ex-b2.png',dpi=600)
#########calculation ends##################

