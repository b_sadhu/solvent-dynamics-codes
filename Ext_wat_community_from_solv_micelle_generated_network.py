import itertools
import copy
import _pickle as cPickle 
import numpy as np


with open('solvent-micelle-network-c3.p','rb') as f:
    b= cPickle.load(f)

#BREAK THE LIST INTO SIX SMALL PARTS
composite_list = [b[x:x+6] for x in range(0, len(b),6)]

print('length of list:',len(composite_list))

sampling_step = 100
trajectory = 100000
multiple = 0

def grouped_list(lista):
    aa = []
    bbc = copy.deepcopy(lista)
    flag = False

    for a, b in itertools.combinations(lista,2):
        bb = a+b

        if len(set(bb)) < len(bb):
            flag = True
            cc = list(set(bb))
            cc.sort()

            if cc not in aa: aa.append(cc)
            if a in lista:   lista.remove(a) 
            if b in lista:   lista.remove(b)

    if lista:        aa = aa + lista
    if not flag:     return bbc
    else:            return grouped_list(aa)


#print ("Grouped list -->", grouped_list(lista))    
ext_wat_community = []
for i in range(0, int(trajectory+sampling_step), sampling_step):
        final_list = []
        multiple += 1
        mol_id = []
        #print('i',i)
        ext_wat_community_per_frame = []
        
        unique_ext_wat_list = []
        for each in range(len(composite_list)):

                wat_cluster = composite_list[each][2]
                prev_wat_cluster = composite_list[each-1][2]
                ext_cluster = composite_list[each][4]
                prev_ext_cluster = composite_list[each-1][4]
                prev_ext_cluster_2 = composite_list[each-2][4]
                time = composite_list[each][0]
                prev_time = composite_list[each-1][0]
                wat_size = composite_list[each][1][0]
                ext_size = composite_list[each][3][0]
                prev_wat_size = composite_list[each-1][1][0]
                prev_ext_size = composite_list[each-1][3][0]
                #NTC ID extraction time wise
                if time == [(multiple*sampling_step)-(sampling_step)]:
                        
                        ext_wat_community_per_frame.append(wat_cluster+ext_cluster)
                            
                elif time == [(multiple*sampling_step)]:
                        break
        #print('frame:',ext_wat_community_per_frame)
       
        union_set = []
        unique_ext_wat_list = grouped_list(ext_wat_community_per_frame)
        ext_wat_community.append(unique_ext_wat_list)
        #print(unique_ext_wat_list)


with open('ext-wat-community-list.p','wb') as wfp2:
	cPickle.dump(ext_wat_community, wfp2, protocol = -1)
