import _pickle as cPickle 
import numpy as np
import MDAnalysis as mda
import os

#####THIS PROGRAM IS INTENDED TO UNDERSTAND THE ROLE OF NITRIC ACID IN DGA CLUSTERING. IT USES THE INPUT FROM 'solv-wat-attached-ntc.py' FOR EXTRACTING ID OF NTC AND THEN USES MDANALYSIS TO COUNT NUMBER OF DGA AROUND THOSE NITRIC ACID WHICH ARE OF-COURSE ATTACHED TO THE WATER CLUSTER. FURTHER, IT FINDS OUT THE REMAINING NTC (WHICH ARE NOT ATTACHED TO WATER CLUSTER) TO CHECK THEIR ROLE AS WELL 

#input for calculations
sampling_step = 100
trajectory = 100000
#0th frame included
num_frames = int(trajectory/sampling_step)+1
top  = 'dt.top'
gro = 'nvt.gro'
tpr = 'nvt.tpr'
xtc = 'nvt-nopbc-100ns.xtc'

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../../%s' % top) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])

id_ntr_start = 0
id_ntr_finish = 0
#read the id number of first extractant
with open('../../../%s' % gro) as fgro:
	lines = fgro.readlines()[1:]
	for line in lines:
		if 'NITRI' in line:
			id_ntr_start = int(line.split('NITRI')[0])
			id_ntr_finish = int(line.split('NITRI')[0]) + num_ntc 
			break

print('nitric_acid_id_start:',id_ntr_start)
print('nitric_acid_id_finish:',id_ntr_finish)

#load whole pbc corrected trajectory as universe
u = mda.Universe('../../../%s' % tpr,'../../../%s' % xtc)

# load the pickle file obtained from solv-wat-attached-ntc.py run
with open('solvent-wat-ntc-network.p','rb') as f:
	b= cPickle.load(f)


###list of zero in file creation###
def zerolistmaker(n):
	listofzeros = [0] * n
	return listofzeros

composite_list = [b[x:x+6] for x in range(0, len(b),6)]
#n = []
#line = []
#used to find multiple instances of same time entry
multiple  = 0
c_ntc = []
count_list = zerolistmaker(3)
for i in range(0, int(trajectory+sampling_step), sampling_step):
	final_list = []
	multiple += 1
	mol_id = []
	print('i',i)
	num_list = zerolistmaker(13)
	#num_list[0] = u.trajectory.frame	
	for each in range(len(composite_list)):
        
		wat_cluster = composite_list[each][2]
		prev_wat_cluster = composite_list[each-1][2]
		ext_cluster = composite_list[each][4]
		prev_ext_cluster = composite_list[each-1][4]
		prev_ext_cluster_2 = composite_list[each-2][4]
		time = composite_list[each][0]
		prev_time = composite_list[each-1][0]
		wat_size = composite_list[each][1][0]
		ext_size = composite_list[each][3][0]
		prev_wat_size = composite_list[each-1][1][0]
		prev_ext_size = composite_list[each-1][3][0]
        
		if time == [(multiple*sampling_step)-(sampling_step)]:
			#line.append(composite_list[each])
			mol_id.append(str(composite_list[each][4])[1:-1].replace("'",""))
       			 
		elif time == [(multiple*sampling_step)]:
			break
	list_id = list(sorted(set(mol_id)))
	list_id_1 = str(list_id).replace("'","").replace("[","").replace("]","").split("\n")
	list_id = str(list_id_1).replace(",","").replace("[","").replace("]","").replace("'","")
	#ntc which are not part of water cluster
	list1 = []
	for each in list_id.split():
		list1.append(int(each))
	set1 = list(np.arange(id_ntr_start, id_ntr_finish, 1))
	set2 = sorted(set(list1))
	not_bonded_ntc_with_water = list(set(set1) - set(set2))
	print('length of not_bonded_ntc:',len(not_bonded_ntc_with_water))
	print('not_bonded_ntc:',not_bonded_ntc_with_water)
	print('length of bonded_ntc:',len(set2))
	print('bonded_ntc:',set2)
	list_id_not_bonded = str(not_bonded_ntc_with_water).replace(",","").replace("[","").replace("]","").replace("'","")
	list_id = str(list(set2)).replace(",","").replace("[","").replace("]","").replace("'","")
	
	for j in u.trajectory[int((i)):int(((i)+1))]:
		num_list[0] = u.trajectory.frame
		print('time:',u.trajectory.frame)
		#FIRST THREE FOR DGA AROUND NITRIC ACIDS WHICH ARE BRIDGED WITH WATER CLUSTER, SECOND THREE IS FOR REST OF NITRIC ACID
		bond_1num_o2 = u.select_atoms("resname DGA and name O2 and (around 3.8 resid %s and name O*)" % (list_id), updating=True).residues
		bond_1num_o3 = u.select_atoms("resname DGA and name O3 and (around 3.8 resid %s and name O*)" % (list_id), updating=True).residues
		bond_1num = len(bond_1num_o2|bond_1num_o3)

		bond_2num_o2 = u.select_atoms("resname DGA and name O2 and (not around 3.8 resid %s and name O*) and (around 10 resid %s and name O*)" % (list_id, list_id), updating=True).residues
		bond_2num_o3 = u.select_atoms("resname DGA and name O3 and (not around 3.8 resid %s and name O*) and (around 10 resid %s and name O*)" % (list_id, list_id), updating=True).residues
		bond_2num = len(bond_2num_o2|bond_2num_o3)

		bond_fnum_o2 = u.select_atoms("resname DGA and name O2 and not (around 10 resid %s and name O*)" % (list_id), updating=True).residues
		bond_fnum_o3 = u.select_atoms("resname DGA and name O3 and not (around 10 resid %s and name O*)" % (list_id), updating=True).residues
		bond_fnum = len(bond_fnum_o2|bond_fnum_o3)

		bond_1num_non_bonded_o2 = u.select_atoms("resname DGA and name O2 and (around 3.8 resid %s and name O*)" % (list_id_not_bonded), updating=True).residues
		bond_1num_non_bonded_o3 = u.select_atoms("resname DGA and name O3 and (around 3.8 resid %s and name O*)" % (list_id_not_bonded), updating=True).residues
		bond_1num_non_bonded = len(bond_1num_non_bonded_o2|bond_1num_non_bonded_o3)

		bond_2num_non_bonded_o2 = u.select_atoms("resname DGA and name O2 and (not around 3.8 resid %s and name O*) and (around 10 resid %s and name O*)" % (list_id_not_bonded, list_id_not_bonded), updating=True).residues
		bond_2num_non_bonded_o3 = u.select_atoms("resname DGA and name O3 and (not around 3.8 resid %s and name O*) and (around 10 resid %s and name O*)" % (list_id_not_bonded, list_id_not_bonded), updating=True).residues
		bond_2num_non_bonded = len(bond_2num_non_bonded_o2|bond_2num_non_bonded_o3)
		
		bond_fnum_non_bonded_o2 = u.select_atoms("resname DGA and name O2 and not (around 10 resid %s and name O*)" % (list_id_not_bonded), updating=True).residues
		bond_fnum_non_bonded_o3 = u.select_atoms("resname DGA and name O3 and not (around 10 resid %s and name O*)" % (list_id_not_bonded), updating=True).residues		
		bond_fnum_non_bonded = len(bond_fnum_non_bonded_o2|bond_fnum_non_bonded_o3)

		#WHOLE FRAME CALCULATIONS (FIRST THREE FOR DGA AROUND NITRIC ACID, SECOND THREE FOR NITRIC ACID AROUND DGA)
		bond_1wh_o2 = u.select_atoms("resname DGA and name O2 and (around 3.8 resname NITRI and name O*)", updating=True).residues
		bond_1wh_o3 = u.select_atoms("resname DGA and name O3 and (around 3.8 resname NITRI and name O*)", updating=True).residues
		bond_1wh = len(bond_1wh_o2|bond_1wh_o3)
		
		bond_2wh_o2 = u.select_atoms("resname DGA and name O2 and (not around 3.8 resname NITRI and name O*) and (around 10 resname NITRI and name O*)", updating=True).residues
		bond_2wh_o3 = u.select_atoms("resname DGA and name O3 and (not around 3.8 resname NITRI and name O*) and (around 10 resname NITRI and name O*)", updating=True).residues
		bond_2wh = len(bond_2wh_o2|bond_2wh_o3)

		bond_fwh_o2 = u.select_atoms("resname DGA and name O2 and not (around 10 resname NITRI and name O*)", updating=True).residues
		bond_fwh_o3 = u.select_atoms("resname DGA and name O3 and not (around 10 resname NITRI and name O*)", updating=True).residues
		bond_fwh = len(bond_fwh_o2|bond_fwh_o3)

		ntr_1num = len(u.select_atoms("resname NITRI and name N* and (around 3.8 resname DGA and name O1 O2 O3)", updating=True).residues)
		ntr_2num = len(u.select_atoms("resname NITRI and name N* and (not around 3.8 resname DGA and name O1 O2 O3) and (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
		ntr_fnum = len(u.select_atoms("resname NITRI and name N* and not (around 10 resname DGA and name O1 O2 O3)", updating=True).residues)
	#print('ntr:',ntr_1num,ntr_2num,ntr_fnum)
	print('num of DGA first_shell_of_ntc:',bond_1num)
	num_list[1] = bond_1num
	print('num of DGA second_shell_of_ntc:',bond_2num)
	num_list[2] = bond_2num
	print('num of DGA beyond_2nd_shell_of_ntc:',bond_fnum)
	num_list[3] = bond_fnum
	print('non_bonded_num of DGA first_shell_of_ntc:',bond_1num_non_bonded)
	num_list[4] = bond_1num_non_bonded
	print('non_bonded_num of DGA second_shell_of_ntc:',bond_2num_non_bonded)
	num_list[5] = bond_2num_non_bonded
	print('non_bonded_num of DGA beyond_2nd_shell_of_ntc:',bond_fnum_non_bonded)
	num_list[6] = bond_fnum_non_bonded
	print('dga around total nitric acid:',bond_1wh, bond_2wh, bond_fwh)
	num_list[7] = bond_1wh
	num_list[8] = bond_2wh
	num_list[9] = bond_fwh
	print('nitric acid around total dga:',ntr_1num,ntr_2num,ntr_fnum)
	num_list[10] = ntr_1num
	num_list[11] = ntr_2num
	num_list[12] = ntr_fnum
	count_ntc = len(list(sorted(set(mol_id))))
	c_ntc.append(count_ntc)
	
	if os.path.exists('role_of_water_bridged_ntc_in_dga_clustering.p'):
		with open('role_of_water_bridged_ntc_in_dga_clustering.p','rb') as rfp:
			final_list = cPickle.load(rfp)
	final_list.append(num_list)
	with open('role_of_water_bridged_ntc_in_dga_clustering.p','wb') as wfp2:
		cPickle.dump(final_list, wfp2, protocol = -1)
	with open('role_of_water_bridged_ntc_in_dga_clustering.txt', "a+") as fp:
		fp.seek(0)
		data = fp.read(100)
		if len(data) > 0:
			fp.write("\n")
		fp.write(str(num_list))
	#counter += 1
	print("ailaaaa")
print('c_ntc:',c_ntc[0])

#percentage of ntc

ntc_percentage = ((np.sum(c_ntc))/(num_ntc*num_frames))*100
print('ntc_percentage:',ntc_percentage)

count_list[0] = [num_ntc]
count_list[1] = c_ntc
count_list[2] = [ntc_percentage]

with open('percentage_id_water_bridged_ntc_in_dga_clustering.p','wb') as wfp2:
	cPickle.dump(count_list, wfp2, protocol = -1)
print('num_ntc:',num_ntc)
print('ntc_percentage:',ntc_percentage)

with open('percentage_id_water_bridged_ntc_in_dga_clustering.txt', "a+") as fp:
	fp.seek(0)
	data = fp.read(100)
	if len(data) > 0:
		fp.write("\n")
	fp.write(str(count_list))
#counter += 1
print("ailaaaa")

