import MDAnalysis
from MDAnalysis.analysis.waterdynamics import SurvivalProbability as SP
import matplotlib.pyplot as plt
import os
import pickle

#directory for files
if os.path.exists('survprob') == False:
	os.mkdir('survprob')
os.chdir('survprob')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../dt.top') as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])
#load into universe
universe = MDAnalysis.Universe('../../nvt_extend.tpr', '../../nvt-extend-nopbc-10ns.xtc')
lSP = []
D1W = {}
#1st coordination shell 0-4 angs for water OW
select1 = "resname SOL and name OW and (around 4 resname DGA and name O1 O2 O3)"
sp = SP(universe, select1, verbose=True)
sp.run(start=0, step=10,tau_max=400)
tau_timeseries_1wat = sp.tau_timeseries
sp_timeseries_1wat = sp.sp_timeseries
lSP.append([sp_timeseries_1wat])
for k in tau_timeseries_1wat:
	D1W[k] = sp_timeseries_1wat[k]
print('dict',D1W)

for tau, sp in zip(tau_timeseries_1wat, sp_timeseries_1wat):
	print("{time} {sp}".format(time=tau, sp=sp))

D2W = {}
#2nd coordination shell 4-10 angs for water OW
select2 = "resname SOL and name OW and (not around 4 resname DGA and name O1 O2 O3) and (around 10 resname DGA and name O1 O2 O3)"
sp = SP(universe, select2, verbose=True)
sp.run(start=0, step=10,tau_max=400)
tau_timeseries_2wat = sp.tau_timeseries
sp_timeseries_2wat = sp.sp_timeseries
lSP.append([sp_timeseries_2wat])
print('list',lSP)
for k in tau_timeseries_2wat:
	D2W[k] = sp_timeseries_2wat[k]
print('dict',D2W)
for tau, sp in zip(tau_timeseries_2wat, sp_timeseries_2wat):
	print("{time} {sp}".format(time=tau, sp=sp))

#free water
DFW = {}
select = "resname SOL and name OW and not (around 10 resname DGA and name O1 O2 O3)"
sp = SP(universe, select, verbose=True)
sp.run(start=0, step=10,tau_max=400)
tau_timeseries_fwat = sp.tau_timeseries
sp_timeseries_fwat = sp.sp_timeseries
for k in tau_timeseries_fwat:
	DFW[k] = sp_timeseries_fwat[k]
for tau, sp in zip(tau_timeseries_fwat, sp_timeseries_fwat):
	print("{time} {sp}".format(time=tau, sp=sp))
print('dfw',DFW)
#load data to pickle as list of dictionaries
LDICT = [D1W,D2W,DFW]
pickle.dump(LDICT, open( "LDICT.p", "wb" ))

if num_ntc > 0:
	#1st coordination shell of NTC
	D1N = {}
	D2N = {}
	DFN = {}
	select = "resname NITRI and name N and (around 4 resname DGA and name O1 O2 O3)"
	sp = SP(universe, select, verbose=True)
	sp.run(start=0, step=10,tau_max=400)
	tau_timeseries_1ntc = sp.tau_timeseries
	sp_timeseries_1ntc = sp.sp_timeseries
	for k in tau_timeseries_1ntc:
		D1N[k] = sp_timeseries_1ntc[k]
	for tau_ntc, sp_ntc in zip(tau_timeseries_ntc, sp_timeseries_ntc):
		print("{time} {sp}".format(time=tau_ntc, sp=sp_ntc))
	#2nd coordination shell of NTC
	select = "resname NITRI and name N and (not around 4 resname DGA and name O1 O2 O3) and (around 10 resname DGA and name O1 O2 O3)"
	sp = SP(universe, select, verbose=True)
	sp.run(start=0, step=10,tau_max=400)
	tau_timeseries_2ntc = sp.tau_timeseries
	sp_timeseries_2ntc = sp.sp_timeseries
	for k in tau_timeseries_2ntc:
		D2N[k] = sp_timeseries_2ntc[k]
	for tau_ntc, sp_ntc in zip(tau_timeseries_2ntc, sp_timeseries_2ntc):
		print("{time} {sp}".format(time=tau_ntc, sp=sp_ntc))
	#free nitric acid
	select = "resname NITRI and name N and not (around 10 resname DGA and name O1 O2 O3)" 
	sp = SP(universe, select, verbose=True)
	sp.run(start=0,step=10,tau_max=400)
	tau_timeseries_fntc = sp.tau_timeseries
	sp_timeseries_fntc = sp.sp_timeseries
	for k in tau_timeseries_fntc:
		DFN[k] = sp_timeseries_fntc[k]
	for tau_ntc, sp_ntc in zip(tau_timeseries_fntc, sp_timeseries_fntc):
		print("{time} {sp}".format(time=tau_ntc, sp=sp_ntc))
	
	#load data to pickle as list of dictionaries
	LDICTWNTC = [D1W,D2W,DFW,D1N,D2N,DFN]
	pickle.dump(LDICTWNTC, open( "LDICTWNTC.p", "wb" ))

# plot
plt.subplot(121)
plt.xlabel('Time')
plt.ylabel('Survival Probability')
plt.xlim((0,4000))
plt.ylim((0,1))
plt.plot(tau_timeseries_1wat, sp_timeseries_1wat, color = 'red')
plt.plot(tau_timeseries_2wat, sp_timeseries_2wat, color='green')
plt.plot(tau_timeseries_fwat, sp_timeseries_fwat, color='blue')
res = not bool(DFW)
if res == True:
 	plt.gca().legend(('Zone I','Zone II'))
if res == False:
	plt.gca().legend(('Zone I','Zone II', 'Zone III'))
plt.savefig('wat_sp_ex-b2.png',dpi=600)

if num_ntc > 0:
	plt.subplot(122)
	plt.xlabel('Time')
	plt.ylabel('Survival Probability')
	plt.xlim((0,400))
	plt.ylim((0,1))
	plt.plot(tau_timeseries_1ntc, sp_timeseries_1ntc, color = 'red')
	plt.plot(tau_timeseries_2ntc, sp_timeseries_2ntc, color='green')
	plt.plot(tau_timeseries_fntc, sp_timeseries_fntc, color='blue')
	plt.gca().legend(('Zone I','Zone II', 'Zone III'))
	plt.savefig('ntc_sp_ex-b2.png',dpi=600)

