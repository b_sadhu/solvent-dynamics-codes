import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import pickle
import MDAnalysis as mda

'''
water micelle cluster connections and corresponding distributions
'''

# traj parameters
traj_length = 100000
sampling_dt = 100
gro = 'nvt.gro'
tpr = 'nvt.tpr'
xtc = 'nvt-nopbc-100ns.xtc'
ndx = 'rdf.ndx'
topol = 'dt.top'
#for DGA-SOL, please indicate the index number of index file which will be used to create gro file
group_name = 'DGA_WAT'
index_group = '29'
#distance cutoff to check the network (in angstrom)
dist = 4
#here water is mol_1 and TODGA is mol_2 (provide the ids of atoms for distance calculation (USES mdanalysis with 'around' feature applying the distance cut-off))
mol_1 = 'OW'
mol_2 = 'O1 O2 O3'
#provide directory name for the calculations, this directory will be created for run
dir_name = 'NUM_SOLV'

#directory for files
if os.path.exists('%s' % dir_name) == False:
	os.mkdir('%s' % dir_name)
os.chdir('%s' % dir_name)

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../%s' % topol) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])


#read the id number of first extractant and water (solvent)
with open('../../%s' % gro) as extgrofile:
	lines = extgrofile.readlines()[1:]
	for line in lines:
		if 'DGA' in line:
			id_ext = int(line.split('DGA')[0])
			print('id_ext:',id_ext)
			break
with open('../../%s' % gro) as extgrofile:
	lines = extgrofile.readlines()[1:]
	for line in lines:
		if 'SOL' in line:
			id_wat = int(line.split('SOL')[0])
			print('id_wat:',id_wat)
			break

###list of zero in file creation###
def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros

#water cluster_pickle_file and extractant cluster pickle file
with open("../../Cluster_analysis/100ps_cut12_lifetime/ecc-clust-outputs/pic_markov_6.txt", "rb") as sp:
	solv_cluster = pickle.load(sp)
with open("../../Cluster_analysis/100ps_cut12_lifetime/ecc-clust-outputs/pic_markov_1.txt", "rb") as fp:
	b = pickle.load(fp)

if os.path.exists('clusterwise-solvent') == False:
	os.mkdir('clusterwise-solvent')

line_count = 0
for each in solv_cluster:
	time = (line_count*sampling_dt)
	print('time:',time)
	num = 0
        #make .gro for each frame
	make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr,'-b','%s' % time, '-e','%s' % time,'-n','../../%s' % ndx,'-o','%s_%s.gro' % (group_name,time)),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = make_gro.communicate(input=('%s \nq\n' % index_group).encode())[0]
	#creating universe file for each frame
	u = mda.Universe('%s_%s.gro' % (group_name,time))
	u.load_new('%s_%s.gro' % (group_name,time))
	for j in range(len(each)):
		listwat = []
		if j > 0:
			prev = len(each[j-1])
			for k in each[j]:
				k += id_wat
				listwat.append(k)
			length = len(listwat)
			idwat = str(listwat)[1:-1].replace( ','  , ' ' )
			if length > 0:
				countlist = []
				countlisttxt = []
				if length == prev:
					num += 1
				else:
					num = 0
				#reading line of extractant cluster file
				for x in range(len(b[line_count])):
					list1 = []
					if x > 0:
						prev = len(b[line_count][x-1])
						for y in b[line_count][x]:
							count = 0
							y += id_ext
							list1.append(y)
							length_dga = len(list1)
							idext = str(list1)[1:-1].replace( ','  , ' ' )
						if length_dga > 1:
							bond_1num = len(u.select_atoms("resid %s and name %s and (around %s resid %s and name %s)" % (str(idwat),mol_1,dist,str(idext), mol_2), updating=True).residues)
							if bond_1num > 0:
								if os.path.exists('clusterwise-solvent/solvent-micelle-network.p'):
									with open('clusterwise-solvent/solvent-micelle-network.p','rb') as rfp:
										countlist = pickle.load(rfp)
								count += 1
								countlist.append([time])
								countlist.append([length])
								countlist.append(listwat)
								countlist.append([length_dga])
								countlist.append(list1)
								countlist.append([count])
								countlisttxt.append("{},{},{},{},{},{}".format([time],[length],listwat,[length_dga],list1,[count]))
								print(countlist)
								pickle.dump(countlist, open("clusterwise-solvent/solvent-micelle-network.p", "wb" ))
								with open('clusterwise-solvent/solv-micelle-network.txt', "a+") as fp:
									fp.seek(0)
									data = fp.read(100)
									if len(data) > 0:
										fp.write("\n")
									fp.write(str(countlisttxt))
						
	line_count += 1
#os.system('rm *.gro')
#########calculation ends##################

