import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import pickle
import MDAnalysis as mda

'''
clusterwise solvent distribution
'''

# traj parameters
traj_length = 100000
sampling_dt = 100
gro = 'nvt.gro'
tpr = 'nvt.tpr'
xtc = 'nvt-nopbc-100ns.xtc'
ndx = 'rdf.ndx'
topol = 'dt.top'

#directory for files
if os.path.exists('NUM_SOLV') == False:
	os.mkdir('NUM_SOLV')
os.chdir('NUM_SOLV')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../%s' % topol) as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])


#read the id number of first extractant
with open('../../%s' % gro) as grofile:
	lines = grofile.readlines()[1:]
	for line in lines:
		if 'DGA' in line:
			id_ext = int(line.split('DGA')[0])
			break


###list of zero in file creation###
def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros

###preparing xyz based on size##
with open("../../Cluster_analysis/100ps_cut12_lifetime/clust-outputs/pic_markov_1.txt", "rb") as fp:
	 b = pickle.load(fp)

counter = 0
if os.path.exists('clusterwise-solvent') == False:
	os.mkdir('clusterwise-solvent')

#num_list = zerolistmaker(10)
for each in b:
	time = (counter*sampling_dt)
	print('time:',time)
	num = 0
        #make .gro for each frame
	#make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../%s' % xtc,'-s','../../%s' % tpr,'-b','%s' % time, '-e','%s' % time,'-n','../../%s' % ndx,'-o','DGA_%s.gro' % (time)),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	#stdin = make_gro.communicate(input=('0 \nq\n').encode())[0]
	#creating universe file for each frame
	u = mda.Universe('DGA_%s.gro' % (time))
	u.load_new('DGA_%s.gro' % (time))
	for j in range(len(each)):
		list1 = []
		if j > 0:
			prev = len(each[j-1])
			for k in each[j]:
				k += id_ext
				list1.append(k)
			length = len(list1)
			idnum = str(list1)[1:-1].replace( ','  , ' ' )
			if length > 1:
				num_list = zerolistmaker(9)
				num_list[0] = time
				clust_solv = []
				clust_solv_all = []
				if length == prev:
					num += 1
				else:
					num = 0
				#Use of MDAnalysis starts: 3 Zone selected for ntc and wat based on RDF
				if num_ntc > 0:
					ntr_1num = len(u.select_atoms("resname NITRI and name N and (around 4 resid %s and name O1 O2 O3)" % str(idnum), updating=True).residues)
					num_list[1] = ntr_1num
					ntr_2num = len(u.select_atoms("resname NITRI and name N and (not around 4 resid %s and name O1 O2 O3) and (around 10 resid %s and name O1 O2 O3)" % (str(idnum),str(idnum)), updating=True).residues)
					num_list[2] = ntr_2num
					ntr_fnum = len(u.select_atoms("resname NITRI and name N and not (around 10 resid %s and name O1 O2 O3)" % str(idnum), updating=True).residues)
					print('ntr:',ntr_1num,ntr_2num,ntr_fnum)
					num_list[3] = ntr_fnum
				if num_wat > 0:
					wat_1num = len(u.select_atoms("resname SOL and name OW and (around 4 resid %s and name O1 O2 O3)" % str(idnum), updating=True).residues)
					num_list[4] = wat_1num
					wat_2num = len(u.select_atoms("resname SOL and name OW and (not around 4 resid %s and name O1 O2 O3) and (around 10 resid %s and name O1 O2 O3)"  % (str(idnum),str(idnum)), updating=True).residues)
					num_list[5] = wat_2num
					wat_fnum = len(u.select_atoms("resname SOL and name OW and not (around 10 resid %s and name O1 O2 O3)" % str(idnum), updating=True).residues)
					num_list[6] = wat_fnum
					print('water:',wat_1num,wat_2num,wat_fnum)
				print('num_list:',num_list)
				
				if os.path.exists('clusterwise-solvent/pic_clusterwise-solvent_%s.p' %str(length)):
					with open('clusterwise-solvent/pic_clusterwise-solvent_%s.p' %str(length),'rb') as rfp:
						clust_solv = pickle.load(rfp)
				#finding if HNO3 is hydrogen bonded with hydrogen bonded water of TODGA
				if os.path.exists('clusterwise-solvent/clusterwise-solvent.p'):
					with open('clusterwise-solvent/clusterwise-solvent.p','rb') as rfp:
						clust_solv_all = pickle.load(rfp)
				if num_ntc > 0 and wat_1num > 0:
					wat_list = u.select_atoms('resname SOL and name OW and (around 4 resid %s and name O1 O2 O3)' % str(idnum), updating=True)
					nparray = str(wat_list.atoms.residues.resids).lstrip('[').rstrip(']')
					#print(nparray)
					ntr_num_near_wat_2p5 = len(u.select_atoms('resname NITRI and name N and (around 4 resid %s and name O*)' % str(nparray)).residues)
					num_list[7] = ntr_num_near_wat_2p5 
				num_list[8] = length
				clust_solv.append(num_list)
				clust_solv_all.append(num_list)
				#cluster wise file binned
				with open('clusterwise-solvent/pic_clusterwise-solvent_%s.p' %str(length),'wb') as wfp2:
					pickle.dump(clust_solv, wfp2)
				#for all cluster in a single file
				pickle.dump(clust_solv_all, open( "clusterwise-solvent/clusterwise-solvent.p", "wb" ))

				with open('clusterwise-solvent/clusterwise-solvent-0-4-10_%s.txt' %str(length), "a+") as fp:
					fp.seek(0)
					data = fp.read(100)
					if len(data) > 0:
						fp.write("\n")
					fp.write(str(num_list))

	counter += 1
#os.system('rm *.gro')
#########calculation ends##################

